# JavaWeb_Cloud_Nacos_AntdVue

#### Description
一款 Java 语言基于 SpringCloud、Nacos、MybatisPlus、Vue3、AntDesign、MySQL等框架精心打造的一款微服务前后端分离框架，可用于快速搭建分布式架构前后端分离后台管理系统，框架已集成了完整的RBAC权限架构和常规基础模块，前端基于Vue、AntDesign等技术栈研发，集成代码生成器可以一键生成模块的增删改查功能，快速提升开发效率；

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
